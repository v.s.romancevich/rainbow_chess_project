package ru.itcube64.chess;

import java.util.ArrayList;

public interface ChessBoardConstant {
    ArrayList<ChessFigure> FIGURES_LIST = new ArrayList<>();
    int WHITE = 0;
    int BLACK = 1;
}
