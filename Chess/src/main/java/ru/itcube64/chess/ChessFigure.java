package ru.itcube64.chess;

import java.awt.*;

public class ChessFigure {
    int x;
    int y;
    Image image;
    int team;

    public ChessFigure(int x, int y, Image image, int team) {
        this.x = x;
        this.y = y;
        this.image = image;
        this.team = team;
    }

}
