package ru.itcube64.chess;

public class ChessGame implements ChessBoardConstant {
    public void startGame() {
        ChessImageParser chessImageParser = new ChessImageParser();
        chessImageParser.parse();

        for (int i = 0; i < 8; i++) {
            FIGURES_LIST.add(new ChessFigure(i, 1, chessImageParser.getImage(BLACK, 5), BLACK));
            FIGURES_LIST.add(new ChessFigure(i, 6, chessImageParser.getImage(WHITE, 5), WHITE));
        }


        for (int i = 0; i < 2; i++) {
            int team = i == 0 ? BLACK : WHITE;
            int y = i == 0 ? 0 : 7;
            FIGURES_LIST.add(new ChessFigure(0, y, chessImageParser.getImage(team, 4), team));
            FIGURES_LIST.add(new ChessFigure(1, y, chessImageParser.getImage(team, 3), team));
            FIGURES_LIST.add(new ChessFigure(2, y, chessImageParser.getImage(team, 2), team));
            FIGURES_LIST.add(new ChessFigure(3, y, chessImageParser.getImage(team, 1), team));
            FIGURES_LIST.add(new ChessFigure(4, y, chessImageParser.getImage(team, 0), team));
            FIGURES_LIST.add(new ChessFigure(5, y, chessImageParser.getImage(team, 2), team));
            FIGURES_LIST.add(new ChessFigure(6, y, chessImageParser.getImage(team, 3), team));
            FIGURES_LIST.add(new ChessFigure(7, y, chessImageParser.getImage(team, 4), team));
        }
        ChessFrame chessFrame = new ChessFrame();
    }


}

