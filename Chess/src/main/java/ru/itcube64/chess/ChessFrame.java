package ru.itcube64.chess;

import javax.swing.*;

public class ChessFrame extends JFrame {

    public ChessFrame() {
        this.setTitle("Chess   Version: Alpha 0.01");
        this.setBounds(50, 50, 526, 549);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        ChessBoard chessBoard = new ChessBoard();

        getContentPane().add(chessBoard);

        this.setVisible(true);
    }


}
