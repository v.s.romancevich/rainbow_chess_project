package ru.itcube64.chess;

import javax.swing.*;
import java.awt.*;

public class ChessBoard extends JPanel implements ChessBoardConstant {

    public ChessBoard() {
        ChessEngine chessEngine = new ChessEngine(this);
        addMouseListener(chessEngine);
        addMouseMotionListener(chessEngine);
    }
    public void paint(Graphics graphics) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Color color = (i + j) % 2 == 0 ? new Color(238, 238, 210) : new Color(118,150,86);
                graphics.setColor(color);
                graphics.fillRect(j * 64, i * 64, 64, 64);
            }
        }
        for (ChessFigure currentShape : FIGURES_LIST) {
            graphics.drawImage(currentShape.image, currentShape.x * 64, currentShape.y * 64, this);
        }
    }
}
