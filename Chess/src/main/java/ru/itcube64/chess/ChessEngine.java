package ru.itcube64.chess;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class ChessEngine implements ChessBoardConstant, MouseListener, MouseMotionListener {
    private ChessFigure selectedShape;
    private final ChessBoard chessBoard;

    public ChessEngine(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int x = e.getX() / 64;
        int y = e.getY() / 64;
        selectedShape = getFigure(x, y);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        int x = e.getX() / 64;
        int y = e.getY() / 64;

        if (selectedShape != null) {
            ChessFigure targetFigure = getFigure(x, y);

            if (targetFigure != null && targetFigure.team != selectedShape.team) {
                if (targetFigure != null) {
                    removeFigure(targetFigure);
                }

                selectedShape.x = x;
                selectedShape.y = y;
                selectedShape = null;
                chessBoard.repaint();
            }
        }
    }

        @Override
        public void mouseDragged (MouseEvent e){
            if (selectedShape != null) {
                int x = e.getX() / 64;
                int y = e.getY() / 64;

                if (canMoveTo(x, y)) {
                    selectedShape.x = x;
                    selectedShape.y = y;
                    chessBoard.repaint();
                }
            }
        }


        @Override
        public void mouseEntered (MouseEvent e){

        }

        @Override
        public void mouseExited (MouseEvent e){
            selectedShape = null;
        }

        @Override
        public void mouseMoved (MouseEvent e){

        }


        @Override
        public void mouseClicked (MouseEvent e){

        }


        private ChessFigure getFigure ( int x, int y){
            for (ChessFigure figure : FIGURES_LIST) {
                if (figure.x == x && figure.y == y) {
                    return figure;
                }
            }
            return null;
        }

        private void removeFigure (ChessFigure figure){
            FIGURES_LIST.remove(figure);
        }

        private boolean canMoveTo ( int x, int y){
            if (selectedShape == null) {
                return false;
            }
            for (ChessFigure figure : FIGURES_LIST) {
                if (figure != selectedShape && figure.x == x && figure.y == y) {
                    return false;
                }
            }
            return true;
        }
    }


